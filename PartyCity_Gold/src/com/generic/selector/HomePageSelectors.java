package com.generic.selector;


public class HomePageSelectors
{
	public static final String header = "o_main-header light";
    public static final String body_topNavLinks = "css,";
    public static final String body_categorySlider = "row one-column width-container-width height-three-by-one height-mobile-three-by-one padding-bottom-normal padding-top-normal margin-bottom-normal margin-top-normal border-bottom-0 border-top-0";
    public static final String body_valuepropPromo = "row one-column width-container-width height-three-by-one height-mobile-two-by-one padding-bottom-normal padding-top-normal margin-bottom-normal margin-top-normal border-bottom-0 border-top-0  bts-slide";
    public static final String body_navTabsContainer = "css,.row.two-column.width-container-width.height-one-by-one.padding-bottom-normal.padding-top-normal.margin-bottom-normal.margin-top-normal.border-bottom-0.border-top-0";
    public static final String bodySection1 =  "row one-column width-container-width height-three-by-one height-mobile-four-by-three padding-bottom-normal padding-top-small margin-bottom-normal margin-top-none border-bottom-0 border-top-0";
    public static final String bodySection2 =  "row one-column width-container-width height-three-by-one height-mobile-same-as-desktop padding-bottom-small padding-top-none margin-bottom-normal margin-top-none border-bottom-0 border-top-0";
    public static final String bodySection3 = "row one-column width-container-width height-three-by-one height-mobile-three-by-one padding-bottom-none padding-top-normal margin-bottom-normal margin-top-normal border-bottom-0 border-top-0";
    public static final String bodySection5 = "row one-column width-container-width height-three-by-one height-mobile-sixteen-by-nine padding-bottom-normal padding-top-normal margin-bottom-normal margin-top-normal border-bottom-0 border-top-0";
    public static final String bodySection6 = "row one-column width-two-thirds-width height-three-by-one height-mobile-four-by-three padding-bottom-normal padding-top-normal margin-bottom-none margin-top-x-small border-bottom-0 border-top-0";
    public static final String bodySection7 = "row one-column width-container-width height-three-by-one height-mobile-three-by-one padding-bottom-normal padding-top-normal margin-bottom-normal margin-top-normal border-bottom-0 border-top-0";
    public static final String bodySection8 = "row one-column width-container-width height-three-by-one height-mobile-three-by-one padding-bottom-none padding-top-none margin-bottom-none margin-top-none border-bottom-0 border-top-0";
    public static final String bodySection9 = "row one-column width-two-thirds-width height-three-by-one height-mobile-four-by-three padding-bottom-normal padding-top-normal margin-bottom-none margin-top-x-small border-bottom-0 border-top-0";
    public static final String bodySection10 = "row three-column width-container-width height-one-by-one height-mobile-one-by-one padding-bottom-normal padding-top-normal margin-bottom-normal margin-top-normal border-bottom-0 border-top-0";
    public static final String bodySection11 = "row one-column width-container-width height-three-by-one height-mobile-three-by-one padding-bottom-normal padding-top-normal margin-bottom-normal margin-top-normal border-bottom-0 border-top-0";
    public static final String footerBottomSection = "o_main-footer dark";
    public static final String footerTopSection = "o_social-footer";
    public static final String footerLogo = "icon icon-pc-tagline";
    public static final String footerEmailSignUp = "css,#block-973-1--1- > footer.o_social-footer > div > div:nth-child(3)";
    public static final String close = "submit dark redirectToCountry ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover";
    public static final String popUp="submit dark redirectToCountry ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only";
    public static final String okBtn="html/body/div[2]/div[3]/div/button";

	
}

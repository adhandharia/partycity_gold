package com.generic.selector;


public class CartSelectors
{
	//done
	public static final String numberOfProducts = "css,#cart-Qty";
	public static final String unitPrice = "css,#cart-table > li > div.item-total-container > div.item-price-container > span";
	public static final String productSubtotal = "css,#cart-table > li > div.item-total-container > span:nth-child(4)";
	public static final String productSubtotalMobile = "price-holder totals-mobile";//yti
	public static final String cartTotals = "m_cart-total-list";
	public static final String checkoutBtn = "css,#checkout-form > div > button";//done tachyon pc
	public static final String shippingMethod = "css,#shipping-method-list";
	public static final String removeItem = "css,#cart-table > li > div.product-info-container > div > button > span";
	
	
	public static final String continueShopping = "dwfrm_cart_continueShopping";
	public static final String orderItemSubtotal = "order-subtotal";
	public static final String orderDiscount = "order-discount discount";
	public static final String orderTotal = "order-value value";
	public static final String totals = "css,#primary > div.block > section > div.checkout-container > ul > li.order-total > span:nth-child(2)";
	public static final String couponField = "dwfrm_cart_couponCode";
	public static final String applyCouponButton = "css,#add-coupon";
//	public static final String couponErrorMessage = "css,#voucherForm .information-message";
	public static final String couponMessage = "css,#voucherForm";
	public static final String couponGlobalMessage = "information-message";
	//public static final String couponPositiveMessage = "information-message positive";
	//public static final String couponWarningMessage = "information-message warning";
	public static final String updateBtn = "dwfrm_cart_updateCart";
	public static final String productQtyBox = "quantity";
	public static final String errorMessage = "information-message warning";
	public static final String postitiveMsg = "information-message positive";
	public static final String removeCoupon = "css,.voucher-code-btn";
	public static final String OrderSubTotal = "test_Order_Totals_Subtotal_$1";
	public static final String glabalErrorMessage = "alert alert-danger alert-dismissable";
	public static final String qty = "quantity_0";
	public static final String productQtyBoxMobile = "css,td>li>div>div>div>form>div>input#quantity_";
	public static final String updateQunatityBtn = "test_Order_Totals_Subtotal_$1";
	public static final String actionMenue = "editEntry_";
	
	public static final String cartContent = "yCmsComponent yComponentWrapper content__empty";
	public static final String cartOrderTotal = "test_cart_totalPrice_label_$";
	public static final String cartOrderShipping = "css,#primary > div.block > section > div.checkout-container > ul > li.order-shipping > span:nth-child(2)";
	public static final String cartOrderTax = "cart-totals-taxes text-right";
	public static final String itemImages = "css,li>div>a>img";
	public static final String itemLink = "css,td>li>div>div>a";
	
}

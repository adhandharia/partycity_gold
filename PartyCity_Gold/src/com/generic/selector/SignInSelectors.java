package com.generic.selector;
public class SignInSelectors
{
	//Done-Tachyon-Partycity
		public static final String userName = "dwfrm_login_username_";
		public static final String  password = "dwfrm_login_password_";
		public static final String emailError = "xpath, //form[@id='dwfrm_login']/fieldset/div/div/span"; 
		public static final String passwordError = "dwfrm_login_password_d0jqraigalur-error";
		public static final String errorMessage = "font-color-red";
		public static final String emailError2 = "html/body/div[1]/main/div[2]/section/div/div[1]/div/div[2]/form/fieldset/div[1]/div/span"; // Done by Tachyon Partycity
		public static final String WelcomeMsg = "a_name";
		public static final String forgotPasswordLnk = "password-reset";
		public static final String forgottenPwdInput = "dwfrm_requestpassword_email";
		public static final String forgotPasswordSubmitBtn = "submit dark";
		public static final String alertPositiveForgottenPassword = "list-title";
		public static final String forgottenPwdEmailError = "dwfrm_requestpassword_email-error";
		public static final String wrongPassword_mail = "css,div>div>form>div.error-form";
		public static final String loginBtn = "dwfrm_login_login";
		//public static final String popUp = "css,.redirectToCountry";
		public static final String popUp="submit dark redirectToCountry ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only";	
		public static final String myAcct_settings = "css,.list>.o_account-tile:nth-child(3)>a>h4>span";
        public static final String WelcomeMsgMobile = "m_main-navigation"; //Done by tachyon
       public static final String mainMenuebutton = "m_main-navigation"; //Done by tachyon

}

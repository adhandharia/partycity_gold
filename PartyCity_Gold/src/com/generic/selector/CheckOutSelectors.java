package com.generic.selector;


public class CheckOutSelectors
{
	
	// Payment Type
	public static final String PaymentTypeSelection_CARD = "PaymentTypeSelection_CARD";
	public static final String PaymentTypeSelection_ACCOUNT = "PaymentTypeSelection_ACCOUNT";
	public static final String PurchaseOrderNumber = "text form-control";
	public static final String PaymentType_continue_button = "btn btn-primary btn-block checkout-next";
	public static final String PaymentTypeCostCenterSelect = "costCenterSelect";
	
	
	//Shipping Address
	public static final String firstName = "dwfrm_singleshipping_shippingAddress_addressFields_firstName";
	public static final String lastName = "dwfrm_singleshipping_shippingAddress_addressFields_lastName";
	public static final String phone = "dwfrm_singleshipping_shippingAddress_addressFields_phone";
	public static final String  address1= "dwfrm_singleshipping_shippingAddress_addressFields_address1";
	public static final String emailAddress = "dwfrm_profile_customer_email";
	public static final String address2="dwfrm_singleshipping_shippingAddress_addressFields_address2";
	public static final String city = "dwfrm_singleshipping_shippingAddress_addressFields_city";
	public static final String state = "dwfrm_singleshipping_shippingAddress_addressFields_states_state";
	public static final String zipcode = "dwfrm_singleshipping_shippingAddress_addressFields_postal";
	public static final String submitShippingAddressBtn = "css,#dwfrm_singleshipping_shippingAddress > section > div.form-row.form-row-button > button > span";
	public static final String pickSuggestedAddrress = "css,#ship-to-address-continue";//done tachyon party city
	public static final String addNewAddress="css,#dwfrm_singleshipping_shippingAddress > section > div.address-selection-section > div.add-edit-address > span:nth-child(2) > a";
	public static final String savedAddressList="css,#dwfrm_singleshipping_addressList";

	public static final String addresses = "dwfrm_singleshipping_addressList";//saved address
	public static final String addAddressChekbx = "Add New Address";
	public static final String addressBookBtn = "css,section>div>div>div>div>div>div>button";//address book button
	public static final String CheckSaveAddress = "SaveAddress";
	public static final String title = "address.title";
	public static final String orderTotalShippingAddress = "css,#summary-holder > div.order-total-container > ul > li.order-total > span:nth-child(2)";
	public static final String orderSubTotalShippingAddress = "css,#summary-holder > div.order-total-container > ul > li.order-subtotal > span:nth-child(2)";
	public static final String fnameError = "dwfrm_singleshipping_shippingAddress_addressFields_firstName-error";
	public static final String lnameError = "dwfrm_singleshipping_shippingAddress_addressFields_lastName-error";
	public static final String address1Error = "dwfrm_singleshipping_shippingAddress_addressFields_address1-error";
	public static final String address2Error = "dwfrm_singleshipping_shippingAddress_addressFields_address1-error";
	public static final String cityError = "dwfrm_singleshipping_shippingAddress_addressFields_city-error";
	public static final String postcodeEerror = "dwfrm_singleshipping_shippingAddress_addressFields_postal-error";
	public static final String alertInfo = "alert alert-danger alert-dismissable";
	public static final String titleError = "titleCode.errors";
	public static final String phoneError = "dwfrm_singleshipping_shippingAddress_addressFields_phone-error";
	public static final String emailError = "dwfrm_profile_customer_email-error";
	
	//shipping method
	//done
	
	public static final String shippingMethod = "shipping-method-1POS_";
	public static final String continueToPaymentMethodBtn = "dwfrm_singleshipping_shippingAddress_save";
	public static final String ShippingMail = "dwfrm_singleshipping_shippingAddress_addressFields_friendshipRewards_email";	
	public static final String addshippingMethodBtn = "change_mode_button";
	public static final String submitShippingMethodbtn = "use_this_delivery_method";
	public static final String orderTotalShippingMethod = "test_cart_totalPrice_label_$";
	public static final String orderSubTotalShippingMethod = "test_Order_Totals_Subtotal_$";
	
	//Payment info
	//done 
	public static final String paypal = "paymentMethodPayPal";
	public static final String cc = "paymentMethodBT";
	public static final String expYear = "dwfrm_billing_paymentMethods_creditCard_expiration_year";//done pc
	public static final String cardNumber = "dwfrm_billing_paymentMethods_creditCard_number_";//done pc
	public static final String expirationMonth = "dwfrm_billing_paymentMethods_creditCard_expiration_month";//done pc
	public static final String cvv = "dwfrm_billing_paymentMethods_creditCard_cvn_";//done pc
	public static final String Bcountery = "address.country";
	public static final String BfirstName = "address.firstName";
	public static final String BlastName = "address.surname";
	public static final String Baddress= "address.line1";
	public static final String Bcity = "address.townCity";
	public static final String Bstate = "address.region";
	public static final String Bzipcode = "address.postcode";
	public static final String Bphone = "address.phone";
	public static final String submitPayment = "css,#dwfrm_billing > section > div.credit-card-section > div.form-row.form-row-button.place-order-button > button";//done pc tachyon
	public static final String orderTotalOrderSumary = "css,#summary-holder > div.order-total-container > ul > li.order-total > span:nth-child(2)"; //Tachyon
	
	
	public static final String selectFirstPayment = "creditCardList";
	public static final String orderReviewBtn = "Continue to review Order";
	public static final String cardtype = "dwfrm_billing_paymentMethods_creditCard_type";
	public static final String expireDay = "new_dwfrm_billing_paymentMethods_creditCard_expiration_month";
	public static final String expireYear = "new_dwfrm_billing_paymentMethods_creditCard_expiration_year";
	public static final String checkSavePayment = "SaveDetails";
	public static final String addPaymentBtn = "css,section>div>div>div>div>div>a.linkarrow";
	public static final String checkSame = "differentAddress";
	public static final String savedPaymentBtn = "btn btn-default btn-block js-saved-payments";
	public static final String orderTotalPymentInfo = "test_cart_totalPrice_label_$";
	public static final String orderShippingPymentInfo = "test_Order_Totals_Delivery_$";
	public static final String orderSubtotalPymentInfo = "test_Order_Totals_Subtotal_$";
	public static final String cardTypeError = "card_cardType.errors";
	public static final String accountNumberError = "card_accountNumber.errors";
	public static final String expirationMonthError = "card_expirationMonth.errors";
	public static final String expirationYearError = "card_expirationYear.errors";
	public static final String cvNumberError = "card_cvNumber.errors";
	public static final String billToCountryError = "billTo_country.errors";
		
	//summary - review / place order
	//done

	public static final String summaryTotal = "order-subtotal";
	public static final String placeOrderBtn = "css,#primary > div > div:nth-child(3) > div > section > div.order-summary-footer > form > fieldset > div > button";
	public static final String shippingCost = "test_Order_Totals_Delivery_$";
	public static final String acceptTerm = "Terms1";
	
	
	//Order confirmation
	//done-tachyon party city
	public static final String orderdetails = "css,#primary > div > div:nth-child(3) > div > section > div.m_order-review-block.mini-billing-address.order-component-block > div > div";
	public static final String shippingAddress ="css,#primary > div > div:nth-child(3) > div > section > div:nth-child(1) > div.details > div";
	public static final String orderId="css,#main > div > section > div.info-container > h2";
	
	public static final String orderConfirmationSubtotal = "order-subtotal";
	//public static final String orderId = "css#main > div > section > div.info-container > h2";
	public static final String orderConfirmationTotal = "test_orderTotal_totalPrice_label_$";
	public static final String orderConfirmationShippingCost = "test_orderTotal_devlieryCost_label_$";
	public static final String orderconfirmationBillingAddress = "col-sm-6 col-md-4 order-billing-address";
	public static final String orderconfirmationshippingAddress = "order-ship-to";
	
	//B2B Order confirmation
	public static final String B2BorderConfirmationTotal = "text-right totals";
	public static final String B2BorderconfirmationBillingAddress = "col-sm-6 order-billing-address";
	public static final String B2BorderconfirmationshippingAddress = "col-md-5 order-ship-to";
	
	//Guest order
	//done
	public static final String choseToLogin = "css,#cart-sign-in-holder .btn-text";
	public static final String returningcustomerUsername = "dwfrm_login_username_";//done pc
	public static final String returningcustomerPassword = "dwfrm_login_password_";//done pc
	public static final String returningcustomerloginBtn = "css,#dwfrm_login > fieldset > div.form-row.form-row-button.submit-container > button";//done pc
	
	public static final String ShippingMethodType = "is-test";
	public static final String guestCheckoutButton = "Checkout as Guest";//done tachyon party city
	public static final String guestMail = "guest.email";
	public static final String guestConfirmationMail = "confirmGuestEmail form-control";
	public static final String guestCreateAccButton = "test_guest_Register_button_$";
	public static final String guestCreateAccOtpin = "consentForm.consentGiven1";
	public static final String guestCreateAccCPwd = "guest.checkPwd";
	public static final String guestCreateAccPwd = "password";
	
}

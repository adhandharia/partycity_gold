package com.generic.selector;

public class MyAccount_PasswordSelectors {
	// Done
	public static final String currentPassword = "profile_login_currentpassword";
    public static final String newPassword = "profile_login_newpassword_";
    public static final String confirmNewPassword = "profile_login_newpasswordconfirm";
    public static final String currentPasswordError ="html//span[contains(@class,'error')]";
    public static final String currentPasswordError2 = "currentPassword.errors";
    public static final String newPasswordError = "html//span[contains(@class,'error')]";
    public static final String newPasswordError2 = "newPassword.errors";
    public static final String confirmNewPasswordError = "html//span[contains(@id,'profile_login_newpasswordconfirm')]";
    public static final String confirmNewPasswordError2 = "html//span[@class='form-captionfont-color-red error-message']";
    public static final String cancelBtn = "CANCEL";
    public static final String updateBtn = "common-button updatepasssword submit dark";
    public static final String globalAlerts = "html//div[@class='o_error']/div/h3";
    public static final String globalAlerts2 = "html//div[@class='o_error']/ul";
    public static final String mainPageAlert = "html//section[@class='p_account wide block']/h5";

    public static final String profileUpdatedMsg = "alert alert-info alert-dismissable";
    



}
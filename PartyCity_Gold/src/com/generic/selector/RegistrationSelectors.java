package com.generic.selector;

public class RegistrationSelectors {
     
     //done OCM
     public static final String firstName = "dwfrm_profile_customer_firstname";
     public static final String lastName = "dwfrm_profile_customer_lastname";
     public static final String emailAddress = "dwfrm_profile_customer_email";
     public static final String school  = "school-search-input";
     public static final String password = "dwfrm_profile_login_password_";
     public static final String confirmPassword = "dwfrm_profile_login_passwordconfirm";
     public static final String terms = "termsAndCondition";
     public static final String addressSection = "css,h3>button.btn-show-hide";
     
     public static String userType = "userType";
     public static final String AddressLine1 = "register.street";
     public static final String city = "address.townCity";
     public static final String state = "register.state";
     public static final String Zipcode = "register.zip";
     public static final String phone = "register.phoneNumber";
     public static final String phoneType = "cellPhone";
     
     
     public static final String registerBtn = "css,#RegistrationForm > fieldset:nth-child(2) > div.form-row.form-row-button > button";
     
     public static final String welcomeMessage = "account-heading";
     
     public static final String alerts = "global-alerts";
     
     //ErrorMessages Selectors
     public static final String emailError = "form-captionfont-color-red error-message";
     public static final String firstNameError = "dwfrm_profile_customer_firstname-error";
     public static final String lastNameError = "dwfrm_profile_customer_lastname-error";
     public static final String emailAddressError = "dwfrm_profile_customer_email-error";
     public static final String schoolError = "school-search-input-error"; 
     public static final String passwordRulesError = "error";
     public static final String confirmPasswordError = "error"; 
     public static final String temsError = "termsAndCondition-error";
     
     
     //pending OCM
     public static final String passwordError = "password_minchar";
     public static final String passwordMatchError = "checkPwd.errors";
     
     public static final String title = "register.title";
     public static final String consentGiven = "consentForm.consentGiven1";
     public static final String register = "btn btn-default btn-block";
     public static final String registrationSuccess = "global-alerts";
     public static final String homePage_MyAccount = "My Account";
     public static final String createNewAccountBtn = "css,#dwfrm_login_register > fieldset > div > button";
     public static final String emailAddressConfirmation = "dwfrm_profile_customer_emailconfirm";
     public static final String birthdayMonth  = "dwfrm_profile_customer_month";
     public static final String birthdayDay = "dwfrm_profile_customer_day";
     
     //ErrorMessages Selectors
     public static final String titleError = "titleCode.errors";
     
}


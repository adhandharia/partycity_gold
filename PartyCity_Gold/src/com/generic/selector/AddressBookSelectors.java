package com.generic.selector;

public class AddressBookSelectors {
	
	
	//done-Partycity
	public static final String addNewAddress = "section-header-note address-create button submit light";
	public static final String addressDetail = "css,li.address-tile";
	public static final String firstName = "dwfrm_profile_address_firstname";
	public static final String lastName = "dwfrm_profile_address_lastname";
	public static final String address= "dwfrm_profile_address_address1";
	public static final String address2= "dwfrm_profile_address_address2";
	public static final String state = "dwfrm_profile_address_states_state";
	public static final String city = "dwfrm_profile_address_city";
	public static final String zipcode = "dwfrm_profile_address_postal";
	public static final String phone = "dwfrm_profile_address_phone";
	public static final String saveAddress = "apply-button submit dark";
	public static final String useCorrectedAddress = ""; //Not Available on Partycity
	public static final String useOriginalAddress = "";  //Not available in particity
	public static final String alertInfo= ""; //Address created successfully  no message in particity
	public static final String selectFirstAddress = "css,.account-addressbook-list>.each-address-holder>.address-info";
	public static final String addressActions = "address-edit link";
	public static final String removeBtn = "address-delete delete link";
	public static final String editAddress = "address-edit link";   //not using for edit....
	public static final String defaultAddress = "address-make-default link";
	public static final String deleteBtn = "address-delete delete link";
	public static final String fnameError = "dwfrm_profile_address_firstname-error"; //Please enter your first name
	public static final String lnameError = "dwfrm_profile_address_lastname-error"; //Please enter your last name
	public static final String address1Error = "dwfrm_profile_address_address1-error"; //Please enter a Street Address
	public static final String cityError = "dwfrm_profile_address_city-error"; // Please enter a city
	public static final String stateError = "dwfrm_profile_address_states_state-error"; // Please select a state
	public static final String postcodeEerror = "dwfrm_profile_address_postal-error"; //Please enter a ZIP code
	public static final String address_holder ="li.address-tile";
	public static final String addedName="mini-address-name"; //locator use to match name
	public static final String addedAddress="mini-address-location"; //locator use to match name
	public static final String totalAddresses="#addresses > ul > li";
	public static final String popUp="ui-button-text";
	
	public static final String clickEditAddress = "dwfrm_profile_address_edit";
	public static final String emailAddress = "email";
	public static final String addressBookBtn = "css,section>div>div>div>div>div>div>button";
	public static final String submitShippingAddressBtn = "btngreen1 btn-primary-green";
	public static final String CheckSaveAddress = "SaveAddress";
	public static final String addressbookurl = "https://hybrisdemo.conexus.co.uk:9002/yacceleratorstorefront/en/my-account/address-book";
	public static final String addaddressurl = "https://hybrisdemo.conexus.co.uk:9002/yacceleratorstorefront/en/my-account/add-address";
	public static final String myaccountBtn = "js-myAccount-toggle";
	public static final String addressbooktBtn = "Address Book";
	public static final String setasdefaultBtn = "account-set-default-address";
	public static final String removeAddress = "action-links removeAddressFromBookButton";
	public static final String cancelAddress = "dwfrm_profile_address_cancel";
	public static final String deleteaddress = "css,body>div>div>div>div>div>div>div>div>div>div>div>a";
	public static final String addressBackBtn = "addressBackBtn";
	public static final String accountAddressbookList = "col-xs-12 col-sm-6 col-md-4 card";
	public static final String country = "";
	

	/*
	//done-ocm
	public static final String addNewAddress = "Create New Address";
	public static final String addressDetail = "css,.account-addressbook-list>.each-address-holder>.address-info";
	public static final String firstName = "address.firstName";
	public static final String lastName = "address.surname";
	public static final String  address= "address.line1";
	public static final String state = "address.region";
	public static final String city = "address.townCity";
	public static final String zipcode = "address.postcode";
	public static final String phone = "address.phone";
	public static final String saveAddress = "btn btn-primary-default change_address_button show_processing_message";
	public static final String useCorrectedAddress = "Use Corrected Address";
	public static final String useOriginalAddress = "Use Original Address";
	public static final String alertInfo= "alert-text"; //Address created successfully
	public static final String selectFirstAddress = "css,.account-addressbook-list>.each-address-holder>.address-info";
	public static final String addressActions = "action-links btn small btn-secondary-default";
	public static final String removeBtn = ".each-address-holder> div > a";
	public static final String editAddress = "Edit";
	public static final String defaultAddress = "Make Default";
	public static final String deleteBtn = "css,.modal.fade.bootstrap-modal.in>div>div>div>div>div>a.left-button-modal";
	public static final String fnameError = "address.firstName-error"; //Please enter your first name
	public static final String lnameError = "address.surname-error"; //Please enter your last name
	public static final String address1Error = "address.line1-error"; //Please enter a Street Address
	public static final String cityError = "address.townCity-error"; // Please enter a city
	public static final String stateError = "address.region-error"; // Please select a state
	public static final String postcodeEerror = "address.postcode-error"; //Please enter a ZIP code
	public static final String address_holder ="each-address-holder";
	
	public static final String clickEditAddress = "dwfrm_profile_address_edit";
	public static final String emailAddress = "email";
	public static final String addressBookBtn = "css,section>div>div>div>div>div>div>button";
	public static final String submitShippingAddressBtn = "btngreen1 btn-primary-green";
	public static final String CheckSaveAddress = "SaveAddress";
	public static final String addressbookurl = "https://hybrisdemo.conexus.co.uk:9002/yacceleratorstorefront/en/my-account/address-book";
	public static final String addaddressurl = "https://hybrisdemo.conexus.co.uk:9002/yacceleratorstorefront/en/my-account/add-address";
	public static final String myaccountBtn = "js-myAccount-toggle";
	public static final String addressbooktBtn = "Address Book";
	public static final String setasdefaultBtn = "account-set-default-address";
	public static final String removeAddress = "action-links removeAddressFromBookButton";
	public static final String cancelAddress = "test_editAddress_cancelAddress_button_$";
	public static final String deleteaddress = "css,body>div>div>div>div>div>div>div>div>div>div>div>a";
	public static final String addressBackBtn = "addressBackBtn";
	public static final String accountAddressbookList = "col-xs-12 col-sm-6 col-md-4 card";
	public static final String country = "";
	*/

}
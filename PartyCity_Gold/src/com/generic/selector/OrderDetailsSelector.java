package com.generic.selector;

public class OrderDetailsSelector {
	//public static final String orderId = "test_orderDetail_overviewOrderID_label_$ span:eq(1)";
	public static final String orderId = "order-number";
	//public static final String orderStatus = "test_orderDetail_overviewOrderStatus_label_$";
	public static final String orderStatus = "item-value";
//	public static final String datePlaced = "test_orderDetail_overviewStatusDate_label_$";
	public static final String datePlaced = "item-value";
	//public static final String orderTotal = "test_orderDetail_overviewOrderTotal_label_$";
	public static final String orderTotal = "order-total";
	public static final String orderItemTotal = "order-subtotal";
	public static final String cancelOrderButton = "cancelOrderButton";
	public static final String returnOrderButton = "returnOrderButton";
	
	public static final String itemlistHeader = "item__list--header";
	public static final String nthResponsiveItemslist = "item__list--item";
	
	public static final String itemPrice = "test_orderDetails_productItemPrice_label_$";
	public static final String itemQty = "qtyValue";
	public static final String itemTotalPrice = "test_orderDetails_productTotalPrice_label_$";
	public static final String itemImages = "css,li>div>div>a>img";
	public static final String itemLink = "css,li>div>div>a";
	
	public static final String ShippingAddress = "order-shipment-address";
	public static final String BillingAddress = "mini-address-location";  
	public static final String PaymentDetails = "orderpaymentinstrumentsgc";
	public static final String DeliveryMethod = "test_orderDetail_deliveryMethod_section_$";
	
	public static final String OrderSumaryOrderTotal = "css,#primary > div.o_order-details > div.order-payment-summary > ul > li.order-total > span:nth-child(2)";
	public static final String OrderSumaryOrderTax = "test_orderTotal_includesTax_label_$";
	public static final String OrderSumaryShippingCost = "test_orderTotal_devlieryCost_label_$";
	public static final String OrderSumaryOrderDiscount = "	test_orderTotal_discount_label_$";
	public static final String OrderSumaryOrderSubtotal = "css,#primary > div.o_order-details > div.order-payment-summary > ul > li.order-subtotal > span:nth-child(2)";
	

}

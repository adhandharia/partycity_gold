package com.generic.selector;

public class PDPSelectors {
	// done-OCM
	public static String price = "price-container";//done party city tachyon
	public static String title = "css,.product-top-content>h1";//done party city tachyon
	public static String addToCartBtn = "Add to Cart";//done party city tachyon
	public static String id = "sku-container";//done party city tachyon
	public static String desc = "m_product-description read-more reveal";//done party city tachyon
	public static String socialMediaButtons = "css,div.st-btns";
	public static String shareBtn = "st-label";
	public static String numberOfProductsInBundle = "html/body//tr";//done party city tachyon
	public static String productsInBundle = "css,div.each-included-item .information";
	public static String reviewsCount = "pr-snippet-read-and-write";//done party city tachyon
	public static String reviewCountPerEachRate = "bv-inline-histogram-ratings-score";
	public static String starsRating = "pr-snippet-rating-decimal";//done party city tachyon
	public static String secondaryRating = "pr-accessible-text";//done party city tachyon
	public static String allColors = "is-light";
	public static String closSocialShareBtn = "st-close";
	public static String PDPnavs = "css,div.nav-tabs-container>ul>li";
	public static String PDPnavs_mobile = "css,div.tab-content>div>div>button";
	public static String favButton = "css,button.favorite";
	public static String randomVariant = "css,.each-option-holder>div>.btn";
	public static String optionHolder = "each-option-holder";
	public static String optionHolderTitle="css,p>span.variant-text";
	public static String readMore = "description-toggle";
	public static String outOfStock = "product-availability out-of-stock";//done by tachyonss

	public static String colorLable = "css,body>div>div>div>div>div>div>div>div>div>ul>li.attribute.color>div.label";
	public static String color = "Select color: ";
	public static String sizeAndFamilyLable = "css,div.product-variations > ul > div > ul > li > div.label";
	public static String lengthLable = "css,div>div>div.product-sister-styles>ul>li>div.label";
	public static String cart_popup = "popup-btn-checkout";
	public static String qty = "input-qty no-spin";
	public static String SA = "stock-in-stock";
	public static String cartPopupProductQty = "quantity";
	public static String miniCartProductUnitPrice = "unit-price";
	public static String minicart = "has-items";//done party city tachyon
	public static String OverView;
	public static String size= "css,.m_variant-swatches.size.squares.tiles.no-toggle>ul";//done party city tachyon

	
}
package com.generic.tests.accountSetup;

import java.text.MessageFormat;
import java.util.Arrays;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import java.util.LinkedHashMap;

import com.generic.page.AddressBook;
import com.generic.page.Cart;
import com.generic.page.CheckOut;
import com.generic.page.PDP;
import com.generic.page.PaymentDetails;
import com.generic.page.Registration;
import com.generic.page.SignIn;
import com.generic.setup.Common;
import com.generic.setup.LoggingMsg;
import com.generic.setup.PagesURLs;
import com.generic.setup.SelTestCase;
import com.generic.tests.addressBook.AddressBookValidationBase;
import com.generic.util.dataProviderUtils;
import com.generic.util.ReportUtil;
import com.generic.util.SASLogger;

public class AccountsSetup extends SelTestCase {

	// user types
	public static final String guestUser = "guest";
	public static final String freshUser = "fresh";
	public static final String loggedInUser = "loggedin";

	// used sheet in test
	public static final String testDataSheet = "AccountSetup";


	private static XmlTest testObject;

	private static ThreadLocal<SASLogger> Testlogs = new ThreadLocal<SASLogger>();

	@BeforeTest
	public static void initialSetUp(XmlTest test) throws Exception {
		Testlogs.set(new SASLogger("Account_setup"));
		testObject = test;
	}

	@DataProvider(name = "Account_Setup", parallel = true)
	public static Object[][] loadTestData() throws Exception {
		// concurrency maintainance on sheet reading
		getBrowserWait(testObject.getParameter("browserName"));

		dataProviderUtils TDP = dataProviderUtils.getInstance();
		Object[][] data = TDP.getData(testDataSheet);
		Testlogs.get().debug(Arrays.deepToString(data).replace("\n", "--"));
		return data;
	}

	@SuppressWarnings("unchecked") // avoid warning from linked hashmap
	@Test(dataProvider = "Account_Setup")
	public void accountSetupBaseTest(String caseId, String runTest, String products, String shippingType,String shippingMethod, String payment,
			String shippingAddress, String billingAddress, String email) throws Exception {
		// Important to add this for logging/reporting
		Testlogs.set(new SASLogger("AccountSetup_" + getBrowserName()));
		setTestCaseReportName("AccountSetup Case");
		logCaseDetailds(MessageFormat.format(LoggingMsg.CHECKOUTDESC, testDataSheet + "." + caseId,
				this.getClass().getCanonicalName(), email, email, payment, shippingMethod));

		LinkedHashMap<String, String> addressDetails = (LinkedHashMap<String, String>) addresses
				.get(shippingAddress);
		
		String Pemail = "";
		LinkedHashMap<String, String> userdetails = null; 
		if (!email.equals(""))
		{
			userdetails = (LinkedHashMap<String, String>) users.get(email);
			Pemail = getSubMailAccount(userdetails.get(Registration.keys.email));
			Testlogs.get().debug("Mail will be used is: " + Pemail);
		}
		
		
		
		try {
			Testlogs.get().debug(Pemail);
			Testlogs.get().debug((String) userdetails.get(Registration.keys.password));
			
			if (getBrowserName().equals("IE"))
				Thread.sleep(2000);
			//Registration.goToMyAccount();
			//Thread.sleep(2000);
			Registration.goToRegistrationForm();
			Thread.sleep(2000);

			Registration.fillAndClickRegister("Accept", "tester", Pemail,
					(String) userdetails.get(Registration.keys.password),
					(String) userdetails.get(Registration.keys.password));
			Thread.sleep(1000);
			String registrationSuccessMsg = Registration.getRegistrationSuccessMessage();
			sassert().assertTrue(registrationSuccessMsg.toLowerCase().contains("My Account"), 
					"Regestration Success, validation failed Expected to have in message: My Account. but Actual message is: " + registrationSuccessMsg);
			
			
			ReportUtil.takeScreenShot(getDriver(), testDataSheet + "_" + caseId);
			Thread.sleep(1000);
			getDriver().get(PagesURLs.getHomePage()+PagesURLs.getAddressBookPage());
			if (getBrowserName().equals("IE"))
				Thread.sleep(2000);
				AddressBook.clickAddNewAddress();
				LinkedHashMap<String, String> addressDetail = (LinkedHashMap<String, String>) addresses.get(shippingAddress);
				AddressBook.fillAndClickSave(addressDetails.get(CheckOut.shippingAddress.keys.firstName),
						 addressDetail.get(CheckOut.shippingAddress.keys.lastName),
						 addressDetail.get(CheckOut.shippingAddress.keys.address),
						 addressDetail.get(CheckOut.shippingAddress.keys.address),
						addressDetail.get(CheckOut.shippingAddress.keys.city),
						addressDetail.get(CheckOut.shippingAddress.keys.city),
						addressDetail.get(CheckOut.shippingAddress.keys.zipcode),
						addressDetail.get(CheckOut.shippingAddress.keys.phone));
			
			
			
			
			getDriver().get(PagesURLs.getPaymentDetailsPage());
			if (getBrowserName().equals("IE"))
				Thread.sleep(2000);
			PaymentDetails.clickOnAddBtn();

			LinkedHashMap<String, String> paymentDetail = (LinkedHashMap<String, String>) paymentCards.get(payment);
             System.out.println(paymentDetail);
			PaymentDetails.fillandClickSave(payment, paymentDetail.get(CheckOut.paymentInnformation.keys.number),
					paymentDetail.get(CheckOut.paymentInnformation.keys.expireMonth),
					paymentDetail.get(CheckOut.paymentInnformation.keys.expireYear), "");
			
			
					
			if(!SignIn.checkUserAccount())
			{
				Testlogs.get().debug("Registeration failed");
				throw new Exception("Registeration failed");
			}
			//SignIn.logIn(Pemail, "1234567");
			if (getBrowserName().equals("IE"))
				Thread.sleep(4000);
			Testlogs.get().debug(MessageFormat.format(LoggingMsg.ADDING_PRODUCT, products.split("\n")[0]));
			LinkedHashMap<String, String> productDetails = (LinkedHashMap<String, String>) invintory
					.get(products.split("\n")[0]);
			PDP.addProductsToCartAndClickCheckOut(productDetails);

			Cart.clickCheckout();
			Thread.sleep(1000);
			// checkout- shipping address
			
			CheckOut.shippingAddress.fillAndClickNext(shippingMethod,
					addressDetails.get(CheckOut.shippingAddress.keys.firstName),
					addressDetails.get(CheckOut.shippingAddress.keys.lastName),
					addressDetails.get(CheckOut.shippingAddress.keys.address), "",
					addressDetails.get(CheckOut.shippingAddress.keys.city),
					addressDetails.get(CheckOut.shippingAddress.keys.city),
					addressDetails.get(CheckOut.shippingAddress.keys.zipcode),
					addressDetails.get(CheckOut.shippingAddress.keys.phone), Pemail);


			// do not save address if scenario is guest user
			boolean saveBilling = true;
			LinkedHashMap<String, String> paymentDetails = (LinkedHashMap<String, String>) paymentCards.get(payment);
			LinkedHashMap<String, String> billAddressDetails = (LinkedHashMap<String, String>) addresses
					.get(billingAddress);

			if (getBrowserName().equals("IE") || getBrowserName().equals("firefox"))
				Thread.sleep(4000);
			// fill billing form and click checkout
			CheckOut.paymentInnformation.fillAndclickNext(payment, "Tester",
					paymentDetails.get(CheckOut.paymentInnformation.keys.number),
					paymentDetails.get(CheckOut.paymentInnformation.keys.expireMonth),
					paymentDetails.get(CheckOut.paymentInnformation.keys.expireYear),
					paymentDetails.get(CheckOut.paymentInnformation.keys.CVCC),
					billAddressDetails.get(CheckOut.shippingAddress.keys.countery),
					billAddressDetails.get(CheckOut.shippingAddress.keys.firstName),
					billAddressDetails.get(CheckOut.shippingAddress.keys.lastName),
					billAddressDetails.get(CheckOut.shippingAddress.keys.address),
					billAddressDetails.get(CheckOut.shippingAddress.keys.city),
					billAddressDetails.get(CheckOut.shippingAddress.keys.zipcode),
					billAddressDetails.get(CheckOut.shippingAddress.keys.phone));

			
			//CheckOut.reviewInformation.clickPlaceOrderBtn();
		
			Common.testPass();
		} catch (Throwable t) {
			setTestCaseDescription(getTestCaseDescription());
			Testlogs.get().debug(MessageFormat.format(LoggingMsg.DEBUGGING_TEXT, t.getMessage()));
			t.printStackTrace();
			String temp = getTestCaseReportName();
			Common.testFail(t, temp);
			ReportUtil.takeScreenShot(getDriver(), testDataSheet + "_" + caseId);
			Assert.assertTrue(false, t.getMessage());
		} // catch
	}// test
}// class

package com.generic.tests.OrderHistory;

import java.text.MessageFormat;
import java.util.Arrays;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import java.util.LinkedHashMap;

import com.generic.page.CheckOut;
import com.generic.page.OrderDetails;
import com.generic.page.OrderDetails.orderSumary;
import com.generic.page.OrderHistory;
import com.generic.page.Registration;
import com.generic.page.SignIn;
import com.generic.setup.Common;
import com.generic.setup.LoggingMsg;
import com.generic.setup.PagesURLs;
import com.generic.setup.SelTestCase;
import com.generic.setup.SheetVariables;
import com.generic.tests.checkout.Base_checkout;
import com.generic.util.dataProviderUtils;
import com.generic.util.ReportUtil;
import com.generic.util.SASLogger;

public class Base_OrderHistory extends SelTestCase {

	// user types
	public static final String guestUser = "guest";
	public static final String freshUser = "fresh";
	public static final String loggedInUser = "Loggedin";
	public static final String loggedDuringChcOt = "logging During Checkout";

	// used sheet in test
	public static final String testDataSheet = SheetVariables.OrderHistory;

	private static XmlTest testObject;

	private static ThreadLocal<SASLogger> Testlogs = new ThreadLocal<SASLogger>();

	@BeforeTest
	public static void initialSetUp(XmlTest test) throws Exception {
		Testlogs.set(new SASLogger("checkout_setup"));
		testObject = test;
	}

	@DataProvider(name = "Orders", parallel = true)
	public static Object[][] loadTestData() throws Exception {
		// concurrency maintenance on sheet reading
		getBrowserWait(testObject.getParameter("browserName"));

		dataProviderUtils TDP = dataProviderUtils.getInstance();
		Object[][] data = TDP.getData(testDataSheet);
		Testlogs.get().debug(Arrays.deepToString(data).replace("\n", "--"));
		return data;
	}

	@SuppressWarnings("unchecked") // avoid warning from linked hashmap
	@Test(dataProvider = "Orders")
	public void OrderHistoryBaseTest(String caseId, String runTest, String desc, String proprties, String products,
			String shippingMethod, String payment, String shippingAddress, String billingAddress, String coupon,
			String email) throws Exception {
		
		
		try {

			Testlogs.set(new SASLogger("OrderHistory_"+getBrowserName()));
			setTestCaseReportName("OrderHistory Case");
			logCaseDetailds(MessageFormat.format(LoggingMsg.CHECKOUTDESC, testDataSheet + "." + caseId,
					this.getClass().getCanonicalName(), desc, proprties.replace("\n", "<br>- "), payment, shippingMethod));
			
			LinkedHashMap<String, String> addressDetails = (LinkedHashMap<String, String>) addresses.get(shippingAddress);
			
			Base_checkout checkoutTest = new Base_checkout();
			checkoutTest.initialSetUp(testObject);
			checkoutTest.external = true; //change this value will not pass through logging
			checkoutTest.checkOutBaseTest(caseId, runTest, desc, proprties, products, shippingMethod, payment, shippingAddress, billingAddress, coupon, email);
			

			String OCbillingAddress = CheckOut.orderConfirmation.getBillingAddrerss();
			String OCOrderItemTotal = CheckOut.orderConfirmation.getItemsSubTotal();
			String OCOrderTotal = CheckOut.reviewInformation .gettotal();	

			logs.debug( OCbillingAddress + OCOrderItemTotal + OCOrderTotal);
			
			
			
			CheckOut.orderConfirmation.placeOrder();
			//String orderId = CheckOut.orderConfirmation.getOrderId();
			
			getDriver().get(PagesURLs.getHomePage()+PagesURLs.getOrderHistoryPage());
			String OHorderId=OrderHistory.getOrderId();		
			String OHOrderTotal=OrderHistory.getOrderTotal();
			
		     
			OrderHistory.clickOrderDetailLink();
			String ODorderId=OrderDetails.getOrderId();
			String ODOrderTotal=orderSumary.getOrderTotal();
			String ODOrderBillingAddress=OrderDetails.getBillingAddrerss();
			String ODPayrment=OrderDetails.getPaymentDetails();
			String ODOrderItemTotal=orderSumary.getOrderSubtotal();
			
			// verifying Order Id
			sassert().assertTrue(OHorderId.contains(ODorderId),"<font color=#f442cb>OrderId is not identical <br> Order History:"
							+ OHorderId.replace("\n", "<br>") + "<br> order Detail:"
							+ ODorderId.replace("\n", "<br>") + "</font>");
		
			// verifying Order total on history and order detail page
			sassert().assertTrue(OHOrderTotal.contains(ODOrderTotal),"<font color=#f442cb>Order Total is not identical <br> Order History:"
					+ OHOrderTotal.replace("\n", "<br>") + "<br> order Detail:"
					+ ODOrderTotal.replace("\n", "<br>") + "</font>");
			
			// verifying billing information
			sassert().assertTrue(
					OCbillingAddress.contains(ODOrderBillingAddress),
					"<font color=#f442cb>Billing addresses is not identical <br> Order confirmtion:"
							+ OCbillingAddress.replace("\n", "<br>") + "<br> order History:"
							+ ODOrderBillingAddress.replace("\n", "<br>") + "</font>");
			

		// verifying order item total
			sassert().assertTrue(OCOrderItemTotal.contains(ODOrderItemTotal),
					"<font color=#f442cb>order total is not identical <br>order confirmation total:<br> " + OCOrderItemTotal
					+ "<br>Order history item total: " + ODOrderItemTotal + "</font>");
					
			
			// verifying order total
			sassert().assertTrue(OHOrderTotal.contains(OCOrderTotal),
					"<font color=#f442cb>order total is not identical <br>Order history total:<br> " + OHOrderTotal
							+ "<br>order confirmation item total:" + OCOrderTotal + "</font>");

			sassert().assertAll();
			Common.testPass();
		} catch (Throwable t) {
			setTestCaseDescription(getTestCaseDescription());
			Testlogs.get().debug(MessageFormat.format(LoggingMsg.DEBUGGING_TEXT, t.getMessage()));
			t.printStackTrace();
			String temp = getTestCaseReportName();
			Common.testFail(t, temp);
			ReportUtil.takeScreenShot(getDriver(), testDataSheet + "_" + caseId);
			Assert.assertTrue(false, t.getMessage());
		} // catch
	}// test
}// class

package com.generic.util;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestEnvPwdProtection {

	private static WebDriver driver = null;
	String browser = "ie";

	@Test
	public void testUsingAutoIT() throws IOException {
		/* calling the AutoIT executable */

		if (browser == "ie") {
			System.setProperty("webdriver.ie.driver", "C:\\eclipse-workspace2\\driver\\ieDriver.exe");	
			driver = new InternetExplorerDriver(); 
			driver.get("https://development-web-partycity.demandware.net/on/demandware.store/Sites-partycity_us2-Site");
		try {
			Thread.sleep(2000L);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Runtime.getRuntime().exec("C:\\eclipse-workspace2\\test2.exe");
		}
		
		else {
			System.setProperty("webdriver.chrome.driver", "C:\\eclipse-workspace2\\driver\\chromeDriver.exe");	
		driver = new ChromeDriver(); 
			driver.get("https://storefront:Partycity123@development-web-partycity.demandware.net/on/demandware.store/Sites-partycity_us2-Site");
		}
	}




}
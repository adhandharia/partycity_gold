package com.generic.page;

import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import com.generic.selector.HomePageSelectors;
import com.generic.setup.EnvironmentFiles;
import com.generic.setup.ExceptionMsg;
import com.generic.setup.LoggingMsg;
import com.generic.setup.SelTestCase;
import com.generic.util.SelectorUtil;

import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;

/**
 * The Class HomePage.
 */
public class HomePage extends SelTestCase {

	public static void prepareBaselineforLogs(String baselines) throws Exception {

		getCurrentFunctionName(true);
		for (String baseline : baselines.split(",")) {

			baseline = baseline + "_" + getBrowserName().replace(" ", "_");
			String VTAs = EnvironmentFiles.getVisualTestingAssetsPath();
			String baselineAbsPath = VTAs + "/" + baseline + ".png";
			String logs_dir = EnvironmentFiles.getLogFilePath();
			File baseLineFile = new File(baselineAbsPath);
			FileUtils.copyFileToDirectory(baseLineFile, Paths.get(logs_dir).toFile());
			String baselinePathInLogs = logs_dir + "/" + baseline + ".png";
			logs.debug("IMAGE:<br><a target=\"_blank\" href=" + baseline + ".png" + "><img src=" + baseline + ".png"
					+ " alt=" + baseline + ".png" + " style=\"width:150px\"></a>");
		}
		getCurrentFunctionName(false);
	}

	public static void updateHeaderBaseline(String baseline) throws Exception {
		List<String> subStrArr = new ArrayList<String>();
		List<String> valuesArr = new ArrayList<String>();
		getCurrentFunctionName(true);
		baseline = baseline + "_" + getBrowserName().replace(" ", "_");
		logs.debug(MessageFormat.format(LoggingMsg.TYPING_ELEMENT_VALUE, "guest mail", baseline));
		subStrArr.add(HomePageSelectors.header);
		valuesArr.add("VisualTesting");
		SelectorUtil.initializeSelectorsAndDoActions(subStrArr, valuesArr);
		String imagePath = EnvironmentFiles.getVisualTestingAssetsPath() + "/" + baseline + ".png";
		ImageIO.write(SelectorUtil.screenShot.get().getImage(), "PNG", new File(imagePath));
		logs.debug("IMAGE:<br><a target=\"_blank\" href=" + baseline + ".png" + "><img src=" + baseline + ".png"
				+ " alt=" + baseline + ".png" + " style=\"width:150px\"></a>");
		getCurrentFunctionName(false);
	}

	// close
	public static void clickClose() throws Exception {
		WebDriver driver = getDriver();
		driver.findElement(By.xpath(HomePageSelectors.okBtn)).click();
		getCurrentFunctionName(false);
	}

	public static void clickpopUp() throws Exception {
		try {
			getCurrentFunctionName(true);
			List<String> subStrArr = new ArrayList<String>();
			List<String> valuesArr = new ArrayList<String>();
			subStrArr.add(HomePageSelectors.popUp);
			valuesArr.add("");
			SelectorUtil.initializeSelectorsAndDoActions(subStrArr, valuesArr);
			getCurrentFunctionName(false);
		} catch (NoSuchElementException e) {
			logs.debug(MessageFormat.format(ExceptionMsg.PageFunctionFailed, new Object() {
			}.getClass().getEnclosingMethod().getName()));
			throw e;
		}
	}

	public static void updateFooterBaseline(String baselines) throws Exception {
		List<String> subStrArr = new ArrayList<String>();
		List<String> valuesArr = new ArrayList<String>();
		getCurrentFunctionName(true);
		int i = 0;
		for (String baseline : baselines.split(",")) {
			baseline = baseline + "_" + getBrowserName().replace(" ", "_");
			logs.debug(MessageFormat.format(LoggingMsg.TYPING_ELEMENT_VALUE, "guest mail", baseline));
			if (baseline.contains("Bottom")) {
				subStrArr.add(HomePageSelectors.footerBottomSection);
			}
			valuesArr.add("VisualTesting");
			SelectorUtil.initializeSelectorsAndDoActions(subStrArr, valuesArr);
			String imagePath = EnvironmentFiles.getVisualTestingAssetsPath() + "/" + baseline + ".png";
			ImageIO.write(SelectorUtil.screenShot.get().getImage(), "PNG", new File(imagePath));
			logs.debug("IMAGE:<br><a target=\"_blank\" href=" + baseline + ".png" + "><img src=" + baseline + ".png"
					+ " alt=" + baseline + ".png" + " style=\"width:150px\"></a>");
			subStrArr.clear();
			valuesArr.clear();
		}
		getCurrentFunctionName(false);
	}

	public static void updateBodyBaseline(String baselines) throws Exception {
		List<String> subStrArr = new ArrayList<String>();
		List<String> valuesArr = new ArrayList<String>();
		getCurrentFunctionName(true);
		for (String baseline : baselines.split(",")) {
			baseline = baseline + "_" + getBrowserName().replace(" ", "_");
			logs.debug(MessageFormat.format(LoggingMsg.TYPING_ELEMENT_VALUE, "guest mail", baseline));
			if (baseline.contains("bodySection1_")) {
				subStrArr.add(HomePageSelectors.bodySection1);
			} else if (baseline.contains("bodySection3_")) {
				subStrArr.add(HomePageSelectors.bodySection3);
			} else if (baseline.contains("bodySection9_")) {
				subStrArr.add(HomePageSelectors.bodySection9);
			} else if (baseline.contains("bodySection10_")) {
				subStrArr.add(HomePageSelectors.bodySection10);
			}
			valuesArr.add("VisualTesting");
			SelectorUtil.initializeSelectorsAndDoActions(subStrArr, valuesArr);
			String imagePath = EnvironmentFiles.getVisualTestingAssetsPath() + "/" + baseline + ".png";
			ImageIO.write(SelectorUtil.screenShot.get().getImage(), "PNG", new File(imagePath));
			logs.debug("IMAGE:<br><a target=\"_blank\" href=" + baseline + ".png" + "><img src=" + baseline + ".png"
					+ " alt=" + baseline + ".png" + " style=\"width:150px\"></a>");
			subStrArr.clear();
			valuesArr.clear();
		}
		getCurrentFunctionName(false);
	}

	public static boolean verifyHeader(String baseline) throws Exception {
		List<String> subStrArr = new ArrayList<String>();
		List<String> valuesArr = new ArrayList<String>();
		getCurrentFunctionName(true);
		baseline = baseline + "_" + getBrowserName().replace(" ", "_");
		logs.debug(MessageFormat.format(LoggingMsg.TYPING_ELEMENT_VALUE, "guest mail", baseline));
		subStrArr.add(HomePageSelectors.header);
		valuesArr.add("VisualTesting");
		SelectorUtil.initializeSelectorsAndDoActions(subStrArr, valuesArr);
		String imagePath = EnvironmentFiles.getLogFilePath() + "/" + baseline + "_actual.png";
		BufferedImage actualImage = SelectorUtil.screenShot.get().getImage();

		ImageIO.write(SelectorUtil.screenShot.get().getImage(), "PNG", new File(imagePath));
		logs.debug("IMAGE:<br><a target=\"_blank\" href=" + baseline + "_actual.png" + "><img src=" + baseline
				+ "_actual.png" + " alt=" + baseline + "_actual.png" + " style=\"width:150px\"></a>");

		String BaseImagePath = EnvironmentFiles.getVisualTestingAssetsPath() + "/" + baseline + ".png";

		BufferedImage expectedImage = ImageIO.read(new File(BaseImagePath));

		ImageDiffer imgDiff = new ImageDiffer();
		ImageDiff diff = imgDiff.makeDiff(actualImage, expectedImage);

		getCurrentFunctionName(false);
		return !diff.hasDiff();
	}

	public static boolean verifyFooter(String baseline) throws Exception {
		List<String> subStrArr = new ArrayList<String>();
		List<String> valuesArr = new ArrayList<String>();
		getCurrentFunctionName(true);
		baseline = baseline + "_" + getBrowserName().replace(" ", "_");
		logs.debug(MessageFormat.format(LoggingMsg.TYPING_ELEMENT_VALUE, "guest mail", baseline));
		if (baseline.contains("Bottom")) {
			subStrArr.add(HomePageSelectors.footerBottomSection);
		}
		valuesArr.add("VisualTesting");
		SelectorUtil.initializeSelectorsAndDoActions(subStrArr, valuesArr);
		String imagePath = EnvironmentFiles.getLogFilePath() + "/" + baseline + "_actual.png";
		BufferedImage actualImage = SelectorUtil.screenShot.get().getImage();

		ImageIO.write(SelectorUtil.screenShot.get().getImage(), "PNG", new File(imagePath));
		logs.debug("IMAGE:<br><a target=\"_blank\" href=" + baseline + "_actual.png" + "><img src=" + baseline
				+ "_actual.png" + " alt=" + baseline + "_actual.png" + " style=\"width:150px\"></a>");

		String BaseImagePath = EnvironmentFiles.getVisualTestingAssetsPath() + "/" + baseline + ".png";

		BufferedImage expectedImage = ImageIO.read(new File(BaseImagePath));

		ImageDiffer imgDiff = new ImageDiffer();
		ImageDiff diff = imgDiff.makeDiff(actualImage, expectedImage);
		subStrArr.clear();
		valuesArr.clear();
		getCurrentFunctionName(false);
		return !diff.hasDiff();
	}

	public static boolean verifyBody(String baselines) throws Exception {
		List<String> subStrArr = new ArrayList<String>();
		List<String> valuesArr = new ArrayList<String>();
		getCurrentFunctionName(true);
		for (String baseline : baselines.split(",")) {
			baseline = baseline + "_" + getBrowserName().replace(" ", "_");
			logs.debug(MessageFormat.format(LoggingMsg.TYPING_ELEMENT_VALUE, "guest mail", baseline));
			if (baseline.contains("bodySection1_")) {
				subStrArr.add(HomePageSelectors.bodySection1);
			} else if (baseline.contains("bodySection3_")) {
				subStrArr.add(HomePageSelectors.bodySection3);
			} else if (baseline.contains("bodySection8_")) {
				subStrArr.add(HomePageSelectors.bodySection8);
			} else if (baseline.contains("bodySection9_")) {
				subStrArr.add(HomePageSelectors.bodySection9);
			} else if (baseline.contains("bodySection10_")) {
				subStrArr.add(HomePageSelectors.bodySection10);
			}
			valuesArr.add("VisualTesting");
			SelectorUtil.initializeSelectorsAndDoActions(subStrArr, valuesArr);
			String imagePath = EnvironmentFiles.getLogFilePath() + "/" + baseline + "_actual.png";
			BufferedImage actualImage = SelectorUtil.screenShot.get().getImage();

			ImageIO.write(SelectorUtil.screenShot.get().getImage(), "PNG", new File(imagePath));
			logs.debug("IMAGE:<br><a target=\"_blank\" href=" + baseline + "_actual.png" + "><img src=" + baseline
					+ "_actual.png" + " alt=" + baseline + "_actual.png" + " style=\"width:150px\"></a>");

			String BaseImagePath = EnvironmentFiles.getVisualTestingAssetsPath() + "/" + baseline + ".png";

			BufferedImage expectedImage = ImageIO.read(new File(BaseImagePath));

			ImageDiffer imgDiff = new ImageDiffer();
			ImageDiff diff = imgDiff.makeDiff(actualImage, expectedImage);
			subStrArr.clear();
			valuesArr.clear();
			getCurrentFunctionName(false);
			return !diff.hasDiff();
		}
		return false;
	}

}
